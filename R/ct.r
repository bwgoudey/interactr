#Contingency table related functions

extractCt <- function(snp_ind, bed_file, y, n_geno=3, step=1000) {
  #Extract the genotype frequencies for a given set of SNP indicies
  #from the given bed file, split based on the given phenotype. 
  if(!is.matrix(snp_ind) && is.numeric(snp_ind))
    snp_ind = matrix(snp_ind,ncol=1)
  
  #If we want to get contingency tables for a lot of SNPs, do in chunks so we don't run out of memory
  if(nrow(snp_ind)>step) {
    chunks = 1:ceiling(nrow(snp_ind)/step)
    if(nrow(snp_ind)>1000) { 
      ct <- do.call("rbind", pblapply( chunks, function(i) 
        extractCt( snp_ind[((i-1)*step+1):min((i*step),nrow(snp_ind)),,drop=F], bed_file, y, n_geno, step=step)))
    } else {
      ct <- do.call("rbind", lapply( chunks, function(i) 
        extractCt( snp_ind[((i-1)*step+1):min((i*step),nrow(snp_ind)),,drop=F], bed_file, y, n_geno, step=step)))
    }
    return(ct)
  }
  
  geno <- extractPlinkGenos(bed_file, snp_ind, length(y), return_format='unpacked')  
  snp_ind_uniq <- sort(unique(matrix(snp_ind,ncol=1)))  
  snp_inds_mapped <- matrix(match(snp_ind, snp_ind_uniq), ncol=ncol(snp_ind))                            
  ct <- genos2Ct(snp_inds_mapped, geno, y, n_geno)
}


genos2Ct <- function(snp_ind, geno, y, n_geno) {
    stopifnot(all(snp_ind>0));
    stopifnot(ncol(geno)==length(y))
    if(!is.matrix(snp_ind))
      snp_ind = matrix(snp_ind, ncol = 1)
    #Covert missing to majority
    geno[geno==3]=NA;
    
    ct_ncol=ncol(snp_ind)
    ct_nrow=nrow(snp_ind)
        
    #Convert our genotypes such that each genotype represents a combination of some sort
    #Anything with a mnissing values gets ignored
    g=matrix(0, nrow = ct_nrow, ncol = ncol(geno));
    for(i in 1:ct_ncol) {
        g=g + geno[snp_ind[,i], ] * n_geno^(i-1)
    }
    
    return(t(sapply(1:nrow(g), function(i) c(tabulate(g[i, y==0]+1, n_geno^ct_ncol), tabulate(g[i, y==1]+1, n_geno^ct_ncol)) )))
}



get3x2From9x2 <-function(ct9x2, n_geno=3) {
  if(!is.matrix(ct9x2)) {
    ct9x2 = matrix(ct9x2,nrow=1)
  }
  
  n_geno_comb = ncol(ct9x2)/2
  ct3x2_1 = cbind(
    matrix(sapply(1:3, function(i) rowSums(ct9x2[, seq(i,n_geno_comb,n_geno), drop=FALSE],1)),nrow=nrow(ct9x2)), 
    matrix(sapply(1:3, function(i) rowSums(ct9x2[, n_geno_comb+seq(i,n_geno_comb,n_geno), drop=FALSE],1)),nrow=nrow(ct9x2))
  )
  ct3x2_2 = cbind(
    matrix(sapply(1:3, function(i) rowSums(ct9x2[, seq(n_geno*(i-1),n_geno*(i)-1)+1, drop=FALSE],1)),nrow=nrow(ct9x2)), 
    matrix(sapply(1:3, function(i) rowSums(ct9x2[, n_geno_comb+seq(n_geno*(i-1),n_geno*(i)-1)+1 , drop=FALSE],1)),nrow=nrow(ct9x2))
  )
  return (list(ct3x2_1, ct3x2_2))
}

