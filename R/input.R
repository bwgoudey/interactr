
#Read in a given list produced by GWIS
readGwisList <- function(gwis_file, var_id) {
  
  #Check file exists
  stopifnot(file.exists(gwis_file))
  
  tab<-read.table(gwis_file, header=T, skip=3)
  tab$rs1<-var_id[tab$prb1]
  tab$rs2<-var_id[tab$prb2]
  return(tab)
}

#' Read in simple config file defining file locations
#'
#' Creates a list object containing named entries to file paths. File format is tab delimited key value pairs. 
#' Very little checking is done here in terms of contents, allowing flexibility for how these files can be used 
#' projects. 
#'
#' @param file string specifying a text file containing key value pairs specifying ocnfiguration info
#' @param path optional string specifying where to add a path variable to fields inherited by the \code{file} config. 
#' @param parent optional string specifying parent config file defining fields which are inherited by the \code{file} config. 
#'
#' @return list object with named entries (from values) to keys definined on config.
#'
#' @examples
#' plot_crayons()
#'
#' @exports
readConfig <- function(f, path=NULL, parent = NULL) {
  #Check file exists
  stopifnot(file.exists(f))
  
  #If parent defined, check it exists
  if(!is.null(parent))
    stopifnot(file.exists(parent))
  
  cfg=list()
  if(!is.null(path))
    path=ifelse(stringr::str_sub(path, start= -1)=='/', path, stringr::str_c(path, "/"))
  cfg$path = path
  
  #Define a small function to parse the config
  parse_config <- function(cfg_file, cfg) {
    cfg_text<-cfg_file[grep('\\w', cfg_file)]
    for(line in cfg_text) {
      #Split line on '=' and surrounding whitespace
      #Sub known cfg vars in the value
      #Add key value to cfg list
      key_val<-strsplit(line, '\\W*=\\W*')[[1]]
      key = key_val[1]
      val = key_val[2]
      vals<-strsplit(val, '[, \\]\t\\[]+', perl=T)[[1]]
      
      I_var = vals %in% names(cfg)
      vals[I_var] = cfg[vals[I_var]]
      
      cfg[[key]] = paste(gsub('[\'\"]', '', vals), sep="", collapse="")
    }
    return(cfg)
  }
  
  #Read  in config, remove empty lines
  if(!is.null(parent)) {
    cfg = parse_config(readr::read_lines(parent), cfg) 
  }
  
  cfg = parse_config(readr::read_lines(f), cfg)
  
  return(cfg)
}
  


#' read_snps
#' This is the core function to read in genotype data from some type of file into some type of R formated data.
#' The nature of the input and output, as well as the subset of the data, is specified by the parameters. 
#' Function is meant to be quite flexbile to allow reading various types of inputs but may have some constraints on which
#' outputs are appropriate depending on the type of variants being input.
#' 
#' @param file The input file. May or may not include the extension depdnging on the file type (e.g. plink files are often split across multiple files so the extension may be excluded). 
#' @param type The type of file. At the moment only 'plink_bin' is accepted. 
#' @param output The type of output: 'df' for a data frame, 'sparse_mat' for a sparse matrix 
#' @param coding 'additive' or 'onehot' 
#' @param ids Can specifiy the SNP ids desired in the output, either by name or index.
#' @param chr Can specify the chromsomes desired in the output. Not compatible with ids
#' @param pos Can specify te base pair position. Requires Chr to be specified either once or per pos. 
#' @param samples Specfify which samples to include in the output, either by name or index
#'
#' @return Genotypes based on parameters
#' @export
#'
#' @examples

read_snps<-function(file, type, output='df', coding='additive', ids=NULL, chr=NULL, pos=NULL, samples=NULL) {
  
  if(type=='plink_bin') {
    bfile=tools::file_path_sans_ext(file)
    snp_annot <-readBim(bfile)
    samp_annot <-readFam(bfile)
    
    if(!is.null(ids) & !is.null(chr))
      stop('read_snps: cannot supply "ids" and "chr" parameters')      
    if(!is.null(pos) & is.null(chr))
      stop('read_snps: cannot supply "pos" parameters without "chr"')      
      
    if(is.null(ids))
      snp_ind=1:nrow(snp_annot)
    if(is.factor(ids) | is.character(ids)) {
      snp_ind=na.omit(match(ids, snp_annot$var_id))
      missing_snps=attr(snp_ind, 'na.action')
      if(length(missing_snps)>0)
        warning(sprintf('%d SNP indicies could not be found: %s', length(missing_snps),
                        paste(ids[missing_snps], collapse=', ')))
    } else if (is.numeric(ids)) {
      if(!all(ids %in% 1:nrow(snp_annot)))
        stop(sprintf('Provided indicies were were outside the range of the SNPs'))
      snp_ind=ids
    }

    genos<-t(extractPlinkGenos(paste(bfile,".bed",sep = ""), snp_ind, n_sample = nrow(samp_annot), 
                      return_format='unpacked', show_pbar=F, na_action='ignore', max_step=1e5))
    colnames(genos)<-snp_annot$var_id[sort(unique(snp_ind))]
    rownames(genos)<-samp_annot$IID
    genos[genos==3]<-NA
    if(!is.null(samples))
      genos=genos[samples, ,drop=F]
  }
  else{
    stop(sprintf('Given input type "%s" is not a valid input type. Valid options include: "plink_bin"\n', type))
  }
    
  genos
}