
#Return a dataframe with x and y coordinates
#for a single contingency table
calcRoc <- function(ct) {   
  
  #Only take in a single contingency table
  stopifnot(!is.matrix(ct) || nrow(ct)==1)

  ct <- matrix(ct, ncol=2)
  ts = colSums(ct)

  xs=ct[,1]/ts[1]
  ys=ct[,2]/ts[2]
  I = order( xs/ys, decreasing = F)
  roc <- data.frame(xs = c(0,cumsum(xs[I])), ys = c(0,cumsum(ys[I])))

  return (roc)

}


#calculate HWE and (approx) p-value for given contingency table(s)
calcHwe <- function(ct3x2, n_geno=3, control_only=T, log.p = T) { 
  #Calculate HWE for all indiv SNPs based on a chi2 test. 
  #We can specify whether we want this over all samples or only controls
  #
  if(control_only==T) {
    ct3x2=ct3x2[, 1:n_geno, drop=F];
  }
  n=rowSums(ct3x2);
  
  #Get frequencies for major and minor counts
  maf=calcMaf(ct3x2);
  
  #calc genotype frequence expected under HWE
  #If contingency table has minor allele first
  I = ct3x2[,1,drop=F] < ct3x2[,3,drop=F]
  e = matrix(0, nrow=length(maf), ncol=3)
  e[I,] = n[I] * cbind((maf[I,drop=F])^2, 2*maf[I,drop=F]*(1-maf[I,drop=F]), (1-maf[I,drop=F])^2);
  e[!I,] = n[!I] * cbind((1-maf[!I])^2, 2*maf[!I]*(1-maf[!I]), maf[!I]^2);

  if(ncol(ct3x2)%%2==0) {
    n_geno_combs = ncol(ct3x2)/2
    ct3x2 = ct3x2[,1:n_geno_combs]+ct3x2[,n_geno_combs+1:n_geno_combs]
  } 
    
  res = data.frame(chi2 = rowSums( ((ct3x2-e)^2)/e))
  res$log10P = -pchisq(res$chi2, df=1, lower.tail = F, log.p = log.p);
  if(log.p){
    res$log10P = res$log10P/log(10)
  }
  
  return(res)
}
  
#calculate minor allele frequencies for given contingency table(s)
calcMaf <- function (ct) {
    n_geno_combs = ncol(ct)/2
    n = rowSums(ct)
    if(ncol(ct)%%2==0) {
      ct = ct[,1:n_geno_combs,drop=F]+ct[,n_geno_combs+1:n_geno_combs,drop=F]
    }
    return( (ct[,2,drop=F] + 2*pmin(ct[,1,drop=F],ct[,3,drop=F])) / (2*n) ) 
}


#calculate missingness for given contingency table(s)
calcMissingness <- function (ct, n_samples,control_only=F) {  
  if(control_only==T) {
    ct=ct[,1:(ncol(ct)/2),drop=F]
  }
  
  return(1 - rowSums(ct)/n_samples)
  
  
}

#calculate Bonfferoni cutoff
calcBonf <- function(items, order) {
  return( -(log10(0.05)-log10(choose(items, order))) )
}

# Taken from Lee et al (2010)
# Its really just a linear model testing whether you get a significant 
# improvement from looking at pairwise with adjacent SNP. This is 
# because a significant improvement is often indicative of differential missingness.
calcLeeQc <- function(ct9x2)
{
  #If we have lots to do, print progress bar
  if(nrow(ct9x2)>50) { 
      return(do.call("rbind", pblapply( 1:nrow(ct9x2), function(i) calcLeeQc( ct9x2[i,,drop=F]))))
  } else if(nrow(ct9x2)>1) {
      return(do.call("rbind", lapply( 1:nrow(ct9x2), function(i) calcLeeQc( ct9x2[i,,drop=F]))))
  }
  
  d=data.frame(X1=(rep(0:2,rep=6)), X2=(rep(rep(0:2,each=3),2)), Y=rep(c(0,1), each=9), freq=ct9x2[1,])
  g1=lm(formula = Y ~ X1, data = d, weights = freq)
  g2=lm(formula = Y ~ X1+X2, data = d, weights = freq)
  return(anova(g1, g2, test="LRT")[2, 5])
}


calcChi2 <- function(ct) {
  eps=0.00001;
  n_geno_combs = ncol(ct)/2
  n=rowSums(ct);
  n0=rowSums(ct[,1:n_geno_combs,drop=F])
  n1=rowSums(ct[,n_geno_combs+(1:n_geno_combs),drop=F])
  
  col_sum = ct[,1:n_geno_combs,drop=F] + ct[,n_geno_combs+(1:n_geno_combs),drop=F]
  e = cbind((col_sum*n0+eps)/n, (col_sum*n1+eps)/n);
  test    = rowSums((ct-e)^2/e,2);
  log10P = -pchisq(test, n_geno_combs-1, lower.tail = F, log.p = T)/log(10);
  return(data.frame(test, log10P))
}


#data already contains variables and an existing x1,x2, y, covars
test_interact <- function(data, dosage=T)
{
  if(dosage) {
    cond_cont=paste(colnames(data)[grep("^(h[0-9]|PC[0-9])", colnames(data))], collapse="+")
    f1 = as.formula(sprintf("y~%s+X1+X2+X1*X2", cond_cont))
    f2 = as.formula(sprintf("y~%s+X1+X2", cond_cont))
    df=1
  } else {
    cond_cont=paste(colnames(data)[grep("^(h[0-9]|PC[0-9])", colnames(data))], collapse="+")
    f1 = as.formula(sprintf("y~%s+factor(X1)+factor(X2)+factor(X1)*factor(X2)", cond_cont))
    f2 = as.formula(sprintf("y~%s+factor(X1)+factor(X2)", cond_cont))
    df=4
  }
  
  Imiss=is.na(data$X1)|is.na(data$X2)
  mF<-glm(f1, family="binomial", data=data[!Imiss,])
  mA<-glm(f2, family="binomial", data=data[!Imiss,])
  
  #calc liklelihood ratio test
  LR <- anova(mA,mF, "lrt")$Deviance[2]
  p=-pchisq(LR,df, lower.tail=F, log=T)/log(10)
  p = ifelse(is.na(p), 0,p)
}


#data already contains variables and an existing x1,x2, y, covars
test_assoc <- function(data, dosage=T)
{
  if(dosage) {
    cond_cont=paste(colnames(data)[grep("^(h[0-9]|PC[0-9])", colnames(data))], collapse="+")
    f1 = as.formula(sprintf("y~%s+X1", cond_cont))
    f2 = as.formula(sprintf("y~%s", cond_cont))
    df=1
  } else {
    cond_cont=paste(colnames(data)[grep("^(h[0-9]|PC[0-9])", colnames(data))], collapse="+")
    f1 = as.formula(sprintf("y~%s+factor(X1)", cond_cont))
    f2 = as.formula(sprintf("y~%s", cond_cont))
    df=2
  }
  
  Imiss=is.na(data$X1)
  mF<-glm(f1, family="binomial", data=data[!Imiss,])
  mA<-glm(f2, family="binomial", data=data[!Imiss,])
  
  #calc liklelihood ratio test
  LR <- anova(mA,mF, "lrt")$Deviance[2]
  p=-pchisq(LR,df, lower.tail=F, log=T)/log(10)
  p = ifelse(is.na(p), 0,p)
}

