
listMemUsage <- function() {
  print(sort( sapply(ls(),function(x){object.size(get(x))})))
}


evalLRTCovarsParallel <- function(ind, njob, plink_file, GSS_file, covar_file) {
    if(!all(sapply(c(paste(plink_file,'.bed',sep=""),GSS_file, covar_file), file.exists))) {
      cat(sprintf("Invalid input\n%s\n", usage))
      stop()
    }
    
    #Read in the plink bim file. ----
    snp_annot <-readBim(plink_file)
    samp_annot <-readFam(plink_file)
    
    #get covariates ready
    covars<-read.table(covar_file,  header = T, '')
    
    #Eval subset of pairs. X1 and X2 are fillers
    data<-merge(covars, samp_annot, X1=samp_annot$y, X2=samp_annot$y)
    
    #Load in GSS pairs. 
    gssRes <- readGwisList(GSS_file, snp_annot$var_id)
    
    start_ind=ceiling((ind-1)*nrow(gssRes)/njob)+1
    end_ind=min(ceiling(ind*nrow(gssRes)/njob), nrow(gssRes))
    
    ps<-t(sapply(start_ind:end_ind, function(i) {
      pair<-c(gssRes$prb1[i], gssRes$prb2[i])
      Xs<-data.frame(t(extractPlinkGenos(paste(plink_file,".bed",sep = ""),
                                         pair, nrow(samp_annot), return_format = "unpacked",show_pbar = T, max_step = 10000)))
      #Form linear combinations based on two current PCs
      data$X1<-Xs$X1
      data$X2<-Xs$X2
      return(c(pair, test_interact(data,dosage = T),test_interact(data,dosage = F)))
    }))
    ps<-data.frame(start_ind:end_ind, ps)
    colnames(ps)<-c("rank", "prb1", "prb2", "dosage", "dummy")
    ps
  }
